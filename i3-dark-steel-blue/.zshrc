#
# ███████╗███████╗██╗  ██╗
# ╚══███╔╝██╔════╝██║  ██║
#   ███╔╝ ███████╗███████║
#  ███╔╝  ╚════██║██╔══██║
# ███████╗███████║██║  ██║
# ╚══════╝╚══════╝╚═╝  ╚═╝
#


### Options ###
setopt correct                                                  # Auto correct mistakes
setopt extendedglob                                             # Extended globbing. Allows using regular expressions with *
setopt nocaseglob                                               # Case insensitive globbing
setopt rcexpandparam                                            # Array expension with parameters
setopt nocheckjobs                                              # Don't warn about running processes when exiting
setopt numericglobsort                                          # Sort filenames numerically when it makes sense
setopt nobeep                                                   # No beep
setopt appendhistory                                            # Immediately append history instead of overwriting
setopt histignorealldups                                        # If a new command is a duplicate, remove the older one
setopt autocd                                                   # if only directory path is entered, cd there.

### Alias ###
alias cp="cp -i"                                                # Confirm before overwriting something
alias df="df -h"                                                # Human-readable sizes
alias free="free -m"                                            # Show sizes in MB
alias ytmp3="youtube-dl -x --audio-format mp3"                  # Download a YouTube video as an mp3 audio file
alias v="nvim"                                                  # Shorter neovim command
alias sv="sudo -E nvim"                                         # Edit file as root but keeping the neovim user config
alias mkdir="mkdir -p"                                          # Create parent directories if they don't exist
alias ls="ls --color"                                           # Always have a colored output
alias ll="ls -l"                                                # Ls with a lot of file information such as permissions
alias la="ls -a"                                                # Normal ls but hidden files are listed too
alias lsd="ls -ld *(-/DN)"                                      # Ls with folders / symlinks only
alias pi="sudo pacman -S"                                       # Pacman install given package(s)
alias pu="sudo pacman -Syu"                                     # ------ system upgrade
alias pr="sudo pacman -Rs"                                      # ------ remove given package(s) and their now useless dependencies
alias ps="pacman -Ss"                                           # ------ search in the package databases
alias psi="pacman -Qs"                                          # ------ ------ -- --- already installed packages
alias pc="sudo pacman -Sc"                                      # ------ clean unused packages and sync database
alias pcc="sudo pacman -Scc"                                    # ------ ----- ALL the cache (/!\ CAN BE DANGEROUS /!\)

### Functions ###
# Git function to add all then commit $1 message and then push it all
gitacp() {
    git add .
    if [ -n "$1" ]; then
        git commit -m "$1"
    else
        git commit -m "Update"
    fi
    git push
}

### Theming ###
autoload -U compinit colors zcalc
compinit -d
colors
# Color man pages (with termcap variables)
export LESS_TERMCAP_mb=$'\E[05;31m'                             # Start blink
export LESS_TERMCAP_md=$'\E[01;32m'                             # Start bold
export LESS_TERMCAP_me=$'\E[0m'                                 # Turn off bold, blink and underline
export LESS_TERMCAP_se=$'\E[0m'                                 # Stop standout
export LESS_TERMCAP_so=$'\E[01;44;30m'                          # Start standout (reverse video)
export LESS_TERMCAP_ue=$'\E[0m'                                 # Stop underline
export LESS_TERMCAP_us=$'\E[04;33m'                             # Start underline
export LESS=-r

### Prompt ###
autoload -U promptinit; promptinit
prompt spaceship                                                # Install it from the AUR (spaceship-prompt-git)
spaceship_vi_mode_disable                                       # Pretty explicit
SPACESHIP_EXEC_TIME_SHOW=false                                  # Disables showing the execution time of last command
setopt prompt_subst                                             # Enable substitution for prompt

### Auto-completion ###
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'       # Case insensitive tab completion
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"         # Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' rehash true                              # Automatically find new executables in path
# Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache
# History settings
HISTFILE=~/.zhistory
HISTSIZE=1000
SAVEHIST=500

### Plugins ###
# Use syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# Use history substring search
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
zmodload zsh/terminfo

### Keybindings ###
bindkey '^[[2~' overwrite-mode                                  # Insert -> Toggle insert mode
bindkey '^[[3~' delete-char                                     # Delete -> Deletes the next character
bindkey '^[[C'  forward-char                                    # → -> Go one character on the right
bindkey '^[[D'  backward-char                                   # ← -> -- --- --------- -- --- left
bindkey '^[[5~' history-beginning-search-backward               # ↑ -> Navigate up in the history
bindkey '^[[6~' history-beginning-search-forward                # ↓ -> Navigate down in the history
# Navigate words
bindkey '^[[1;5C' forward-word                                  # Ctrl+→ -> Goto next word
bindkey '^[[1;5D' backward-word                                 # Ctrl+← -> Goto previous word
bindkey '^H' backward-kill-word                                 # Ctrl+backspace -> Delete previous word
bindkey '^[[Z' undo                                             # Shift+tab -> Undo last action
# History substring search
bindkey "$terminfo[kcuu1]" history-substring-search-up          # ↑ -> Try to find a similar command up in the history
bindkey '^[[A' history-substring-search-up                      # ↑ -> --- -- ---- - ------- ------- -- -- --- -------
bindkey "$terminfo[kcud1]" history-substring-search-down        # ↓ -> --- -- ---- - ------- ------- down in the history
bindkey '^[[B' history-substring-search-down                    # ↓ -> --- -- ---- - ------- ------- ---- -- --- -------

### Other settings ###
WORDCHARS=${WORDCHARS//\/[&.;]}                                 # Don't consider certain characters part of the word
PROMPT_EOL_MARK=''                                              # Removes the trailing % at the end of newlines
export SUDO_PROMPT=$'\e[33mPassword:\e[0m '                     # Make the sudo prompt simpler and colorful

