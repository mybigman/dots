#!/bin/bash

rofi_command="rofi -theme themes/powermenu.rasi"

### Options ###
lock=""
suspend="鈴"
power_off=""
reboot=""
log_out=""
# Variable passed to rofi
options="$lock\n$suspend\n$power_off\n$reboot\n$log_out"

chosen="$(echo -e "$options" | $rofi_command -dmenu)"
case $chosen in
    $lock)
        light-locker-command -l
        ;;
    $suspend)
        mpc -q pause
        amixer set Master mute
        #systemctl suspend
        xfce4-session-logout --suspend
        ;;
    $power_off)
        #systemctl poweroff
        xfce4-session-logout --halt
        ;;
    $reboot)
        #systemctl reboot
        xfce4-session-logout --reboot
        ;;
    $log_out)
        #i3-msg exit
        xfce4-session-logout --logout
        ;;
esac

