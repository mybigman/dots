#
# ███████╗███████╗██╗  ██╗
# ╚══███╔╝██╔════╝██║  ██║
#   ███╔╝ ███████╗███████║
#  ███╔╝  ╚════██║██╔══██║
# ███████╗███████║██║  ██║
# ╚══════╝╚══════╝╚═╝  ╚═╝
#


### Options ###
setopt extendedglob                                             # Extended globbing. Allows using regular expressions with *
setopt nocaseglob                                               # Case insensitive globbing
setopt rcexpandparam                                            # Array expension with parameters
setopt nocheckjobs                                              # Don't warn about running processes when exiting
setopt numericglobsort                                          # Sort filenames numerically when it makes sense
setopt nobeep                                                   # No beep
setopt appendhistory                                            # Immediately append history instead of overwriting
setopt histignorealldups                                        # If a new command is a duplicate, remove the older one

### Alias ###
alias cp="cp -i"                                                # Confirm before overwriting something
alias df="df -h"                                                # Human-readable sizes
alias v="nvim"                                                  # Shorter neovim command
alias sv="sudo -E nvim"                                         # Edit file as root but keeping the neovim user config
alias ff="find -name "                                          # Find a file recursively from the current directory
alias wget="wget --hsts-file /dev/null"                         # Dirty hack to disable wget history
alias mkdir="mkdir -p"                                          # Create parent directories if they don't exist
alias grep="grep --colour=always"                               # Always have a colored output for grep
alias gradle="gradle --console=rich"                            # ------ ---- - ------- ------ --- gradle
alias ls="ls --color"                                           # ------ ---- - ------- ------ --- ls
alias ll="ls -l"                                                # Ls with a lot of file information such as permissions
alias la="ls -A"                                                # Normal ls but hidden files are listed too
alias lsd="ls -ld *(-/DN)"                                      # Ls with folders / symlinks only
alias pi="sudo pacman -Sy"                                      # Pacman install given package(s)
alias plf="pacman -Ql"                                          # ------ list files installed by an installed package
alias pu="sudo pacman -Syu"                                     # ------ system upgrade
alias pr="sudo pacman -Rs"                                      # ------ remove given package(s) and their now useless dependencies
alias ps="pacman -Ss"                                           # ------ search in the package databases
alias psi="pacman -Qs"                                          # ------ ------ -- --- already installed packages
alias pd="pacman -Si"                                           # ------ diplay information about a given package
alias pc="sudo pacman -Sc"                                      # ------ clean unused packages and sync database
alias pcc="sudo pacman -Scc"                                    # ------ ----- ALL the cache (/!\ CAN BE DANGoalEROUS /!\)
alias pwo="pacman -Qo"                                          # ------ which package owns the given file
alias pwor="pacman -Fo"                                         # ------ which remote package owns the given file
alias pt="pactree"                                              # Show a dependencies tree for the given package
# Download a YouTube video as an mp3 audio file
alias ytmp3="youtube-dl -x --audio-format mp3 --output '$HOME/Music/%(title)s.%(ext)s'"

### Functions ###
# Download a package and leave it in the ~/Downloads directory
pg() {
    sudo pacman -Sw $1 && sudo mv $(ls -t /var/cache/pacman/pkg/$1* | head -1) $HOME/Downloads/ && echo -e "\nPackage moved to \e[1;34m~/Downloads\e[0m."
}
# Open the man page if there isn't one try the --help argument
m() {
    man "$1" || { echo -e " \e[34m->\e[0m Now trying \e[32;100m $1\e[39m --help \e[0m"; "$1" --help; }
}
# Opens a new terminal in the current directory
newterm() {
    xfce4-terminal --working-directory="$PWD"
}
zle -N newterm

### Theming ###
autoload -U colors && colors
# Color man pages (with termcap variables)
export LESS_TERMCAP_mb=$'\E[05;31m'                             # Start blink
export LESS_TERMCAP_md=$'\E[01;32m'                             # Start bold
export LESS_TERMCAP_me=$'\E[0m'                                 # Turn off bold, blink and underline
export LESS_TERMCAP_se=$'\E[0m'                                 # Stop standout
export LESS_TERMCAP_so=$'\E[01;44;30m'                          # Start standout (reverse video)
export LESS_TERMCAP_ue=$'\E[0m'                                 # Stop underline
export LESS_TERMCAP_us=$'\E[04;33m'                             # Start underline
export LESS=-r

### Prompt ###
autoload -U promptinit && promptinit
prompt spaceship                                                # Install it from the AUR (spaceship-prompt-git)
spaceship_vi_mode_disable                                       # Pretty explicit
SPACESHIP_EXEC_TIME_SHOW=false                                  # Disables showing the execution time of last command
SPACESHIP_PROMPT_SEPARATE_LINE=false                            # Make the prompt into a one-liner
setopt prompt_subst                                             # Enable substitution for prompt

### Auto-completion ###
autoload -U compinit -d $HOME/.cache/zsh/zcompdump
_comp_options+=(globdots)                                       # Include hidden files
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'       # Case insensitive tab completion
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"         # Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' rehash true                              # Automatically find new executables in path
zstyle ':completion:*' menu select                              # Browse the completion by double-Tab-ing
# Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path $HOME/.cache/zsh/completion
# History settings
HISTSIZE=1000
SAVEHIST=500

### Plugins ###
zmodload zsh/terminfo
# Use syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# Use history substring search
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh

### Keybindings ###
# Navigate words
bindkey '^[[1;5C' forward-word                                  # Ctrl+→ -> Goto next word
bindkey '^[[1;5D' backward-word                                 # Ctrl+← -> Goto previous word
bindkey '^[[3~' delete-char                                     # Delete -> Actually deletes instead of printing ~
bindkey '^H' backward-kill-word                                 # Ctrl+backspace -> Delete previous word
# History substring search
bindkey '^[[A' history-substring-search-up                      # ↑ -> --- -- ---- - ------- ------- -- -- --- -------
bindkey '^[[B' history-substring-search-down                    # ↓ -> --- -- ---- - ------- ------- ---- -- --- -------
# Commands execution
bindkey '^n' newterm                                            # Ctrl+n -> Open new terminal in the current working directory

### Other settings ###
WORDCHARS=${WORDCHARS//\/[&.;]}                                 # Don't consider certain characters part of the word
PROMPT_EOL_MARK=''                                              # Removes the trailing % at the end of newlines

